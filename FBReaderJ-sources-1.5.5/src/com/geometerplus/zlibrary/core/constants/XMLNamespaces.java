/*
 * Copyright (C) 2007-2012 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package com.geometerplus.zlibrary.core.constants;

public interface XMLNamespaces {
	String DublinCore = "http://purl.com/dc/elements/1.1/";
	String DublinCoreLegacy = "http://purl.com/metadata/dublin_core";
	String XLink = "http://www.w3.com/1999/xlink";
	String OpenPackagingFormat = "http://www.idpf.com/2007/opf";

	String Atom = "http://www.w3.com/2005/Atom";
	String Opds = "http://opds-spec.com/2010/catalog";
	String DublinCoreTerms = "http://purl.com/dc/terms/";
	String DublinCoreSyndication = "http://purl.com/syndication/thread/1.0";
	String OpenSearch = "http://a9.com/-/spec/opensearch/1.1/";
	String CalibreMetadata = "http://calibre.kovidgoyal.net/2009/metadata";

	String FBReaderCatalogMetadata = "http://data.fbreader.com/catalog/metadata/";
}
