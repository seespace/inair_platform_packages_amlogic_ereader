/*
 * This code is in the public domain.
 */

package com.geometerplus.android.fbreader.libraryService;

interface LibraryInterface {
	boolean isUpToDate();
}
