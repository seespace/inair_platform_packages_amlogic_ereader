/*
 * Copyright (C) 2010-2012 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package com.geometerplus.android.fbreader.network.action;

import android.app.Activity;

import com.geometerplus.fbreader.network.NetworkTree;
import com.geometerplus.fbreader.network.NetworkLibrary;
import com.geometerplus.fbreader.network.tree.NetworkCatalogTree;

import com.geometerplus.android.fbreader.network.Util;

public abstract class Action {
	public final int Code;
	public final int IconId;

	protected final Activity myActivity;
	private final String myResourceKey;

	protected Action(Activity activity, int code, String resourceKey, int iconId) {
		myActivity = activity;
		Code = code;
		myResourceKey = resourceKey;
		IconId = iconId;
	}

	public abstract boolean isVisible(NetworkTree tree);

	public boolean isEnabled(NetworkTree tree) {
		return true;
	}

	public abstract void run(NetworkTree tree);

	public String getContextLabel(NetworkTree tree) {
		return
			NetworkLibrary.resource().getResource(myResourceKey).getValue();
	}

	public String getOptionsLabel(NetworkTree tree) {
		return
			NetworkLibrary.resource().getResource("menu").getResource(myResourceKey).getValue();
	}
}
